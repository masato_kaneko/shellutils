#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -euxv

db_user="${1}"
db_name="${2}"

if [ ${#} -eq 3 ]; then
	db_port="${3}"
else
	db_port="3306"
fi

dest_dir=$(mktemp -d)
dest_fname="mysqldump-${db_name}-$(date '+%Y%m%dT%H%M%S').sql.bz2"

cd "${dest_dir}"

## Execute mysqldump
mysqldump --add-drop-table -u "${db_user}" -P "${db_port}" -p "${db_name}" \
	| bzip2 -9 -c  >"${dest_fname}"

## Print full path of the backup file
printf -- "Dump file saved as: ${dest_dir}/${dest_fname}\n"

# [EOF]

#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

: ${1}

path_wp_config="${1}"

cat "${path_wp_config}" \
	| tr -d "\r" \
	| grep "define" \
	| grep "DB_" \
	| sed -e "s/define('//g" -e "s/', /=/g" -e 's!);!!g'


# [EOF]

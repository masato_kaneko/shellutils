#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

path_wp_config="${1}"
path_mysqldump="${2}"

db_info=$(mktemp)

cleanup() {
	rm -f "${db_info}"
}

## Clean Up
trap cleanup EXIT 1 2 3 15

: "Parse wp-config.php and extract database information" && {
	cat "${path_wp_config}" \
		| tr -d "\r" \
		| grep "define" \
		| grep "DB_" \
		| sed -e "s/define('//g" -e "s/', /=/g" -e 's!);!!g'  >"${db_info}"
}

. "${db_info}"

: "Restore mysqldump into new WordPress database" && {
	extension=$(printf -- "${path_mysqldump}" | tr '.' "\n" | tail -1)

	printf -- "Password => %s\n" ${DB_PASSWORD}
	if [ "${extension}" = "bz2" ]; then
		bzcat "${path_mysqldump}" \
			| mysql -u "${DB_USER}" -h "${DB_HOST}" -p "${DB_NAME}"
	fi
}

# [EOF]

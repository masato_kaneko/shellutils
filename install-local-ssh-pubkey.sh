#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

if [ ${#} -ne 1 ]; then
	printf -- "Usage: ${0} <username>\n" 1>&2
	exit 1
fi

username="${1}"

set -- $(cat /etc/passwd | grep "${username}" | cut -d: -f3,4,6 | tr ':' ' ')
user_id="${1}"
group_id="${2}"
path_homedir="${3}"

path_sshdir="${path_homedir}/.ssh"
path_authkeys="${path_sshdir}/authorized_keys"

root_flag=0

if [ $(id -u) -eq 0 ]; then
	root_flag=1
fi

if [ ! -d "${path_sshdir}" ]; then
	mkdir -p "${path_sshdir}"
	chmod 700 "${path_sshdir}"
fi

printf -- "Input SSH public key string then enter EOF >>\n"

cat  >>"${path_authkeys}"
chmod 600 "${path_authkeys}"

if [ ${root_flag} -eq 1 ]; then
	chown -R "${user_id}:${group_id}" "${path_sshdir}"
fi

# [EOF]

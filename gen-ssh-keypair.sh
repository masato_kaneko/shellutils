#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

if [ ${#} -ne 1 ]; then
	printf -- "Usage: ${0} <keyname>\n" 1>&2
	exit 1
fi

keyname="${1}"
TIMESTAMP=$(date '+%s-%Y%m%d%H%M%S');

ssh-keygen -m PEM -t rsa -b 4096 -N "" -C "${keyname}-${TIMESTAMP}" -f "${keyname}-${TIMESTAMP}"

# [EOF]

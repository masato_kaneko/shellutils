#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

path_wp_config="${1}"
timestamp=$(date '+%Y-%m-%d-%H%M')
dest_fname='mysqldump-${DB_NAME}-${timestamp}.sql.bz2'

dest_dir="mysqldump_data"
tempfile_dir=$(mktemp -d)
db_credentials="${tempfile_dir}/db_credentials.txt"
mysql_conf="${tempfile_dir}/mysql.cnf"

cleanup() {
	rm -rf "${tempfile_dir}" >/dev/null 2>&1
}

## Clean Up
trap cleanup EXIT 1 2 3 15

## Make destination directory if it does not exist
if [ ! -d "${dest_dir}" ]; then
	mkdir -p "${dest_dir}"
fi

## Parse wp-config.php and extract database information
cat "${path_wp_config}" \
	| tr -d "\r" \
	| grep "define" \
	| grep "DB_" \
	| sed -e "s/\s*define\s*(\s*'//g" -e "s/', /=/g" -e 's!);!!g' \
	>"${db_credentials}"

## Generate a MySQL config file
. "${db_credentials}"
dest_fname=$(eval printf -- ${dest_fname})
cat <<_EOT_ >"${mysql_conf}"
[client]
user = ${DB_USER}
password = ${DB_PASSWORD}
host = ${DB_HOST}
_EOT_

## Execute mysqldump
mysqldump --defaults-extra-file="${mysql_conf}" --add-drop-table "${DB_NAME}" \
	| bzip2 -9 -c  >"${dest_dir}/${dest_fname}"

# [EOF]
